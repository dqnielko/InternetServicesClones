package com.authorization.service.jwt;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@RefreshScope
public class JwtUtils {
  @Value("${jwt.secret}")
  private String jwtSecret;
  @Value("${jwt.expiration}")
  private int jwtExpirationMs;
  public String generateToken(String username){
    return Jwts.builder().setSubject(username)
            .setIssuedAt(new Date())
            .setExpiration(new Date(new Date().getTime()+jwtExpirationMs))
            .signWith(SignatureAlgorithm.HS256,jwtSecret)
            .compact();
  }
  public String getUsernameFromToken(String token){
    return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody()
            .getSubject();
  }
  public boolean validateToken(String token){
    try {
      Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
      return true;
    } catch (SignatureException e) {
      System.out.println("Signature exception "+e.getMessage());
    } catch (MalformedJwtException e) {
      System.out.println("Invalid jwt token "+e.getMessage());
    } catch (ExpiredJwtException e) {
      System.out.println("Jwt token expired "+e.getMessage());
    } catch (UnsupportedJwtException e) {
      System.out.println("Jwt token is not supported "+e.getMessage());
    } catch (IllegalArgumentException e) {
      System.out.println("Jwt claims string is empty "+e.getMessage());
    }
    return false;
  }
}
