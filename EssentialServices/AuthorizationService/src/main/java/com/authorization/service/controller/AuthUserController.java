package com.authorization.service.controller;

import com.authorization.service.dto.AuthUserDto;
import com.authorization.service.jwt.JwtUtils;
import com.authorization.service.service.AuthUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthUserController {
  private final AuthUserService authUserService;
  private final JwtUtils jwtUtils;
  @PostMapping("/register")
  public ResponseEntity<String> registerUser(@RequestBody AuthUserDto authUserDto){
    ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
    if(authUserService.addUser(authUserDto)){
      responseEntity = ResponseEntity.ok().build();
    }
    return responseEntity;
  }
  @PostMapping("/login")
  public ResponseEntity<String> loginUser(@RequestBody AuthUserDto authUserDto){
    ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
    if(authUserService.validateUser(authUserDto)){
      String jwt = jwtUtils.generateToken(authUserDto.getUsername());
      responseEntity = ResponseEntity.ok(jwt);
    }
    return responseEntity;
  }
}
