package com.authorization.service.service;

import com.authorization.service.entity.AuthUser;
import com.authorization.service.entity.Role;
import com.authorization.service.dto.AuthUserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.authorization.service.repository.AuthUserRepository;

import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthUserService {
  private final AuthUserRepository authUserRepository;
  private final PasswordEncoder passwordEncoder;
  public boolean addUser(AuthUserDto authUserDto){
    Optional<AuthUser> authUserOptional = authUserRepository.findByUsername(authUserDto.getUsername());
    boolean result = false;
    if(authUserOptional.isEmpty()){
      AuthUser authUser = AuthUser.builder().username(authUserDto.getUsername())
                      .password(passwordEncoder.encode(authUserDto.getPassword()))
                      .roles(Set.of(Role.builder().name("ROLE_USER").build()))
                      .build();
      authUserRepository.save(authUser);
      result = true;
    }
    return result;
  }
  public boolean validateUser(AuthUserDto authUserDto){
    boolean result = false;
    Optional<AuthUser> authUserOptional = authUserRepository.findByUsername(authUserDto.getUsername());
    if(authUserOptional.isPresent()&&
            passwordEncoder.matches(authUserDto.getPassword(),authUserOptional.get().getPassword())){
      result = true;
    }
    return result;
  }
}
