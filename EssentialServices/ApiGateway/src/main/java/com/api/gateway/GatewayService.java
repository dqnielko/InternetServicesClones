package com.api.gateway;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class GatewayService {
  @Bean
  public RouteLocator createRouteLocator(RouteLocatorBuilder routeLocatorBuilder){
    return routeLocatorBuilder.routes()
            .route(r->
                    r.path("/login/**")
                            .uri("http://localhost:8081"))
            .route(r->
                    r.path("/register/**")
                            .uri("http://localhost:8081"))
            .route(r->
                    r.path("/storage/**")
                            .uri("http://localhost:8082"))
    .build();
  }
}
