package com.example.storageservice.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileInfo {
  @Id
  @GeneratedValue
  private Long id;
  @Column
  private String name;
  @Column
  private String path;
  @Column
  private Date date;
}
