package com.example.storageservice.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StorageUser {
  @Id
  @GeneratedValue
  private Long id;
  @Column
  private String username;
  @OneToMany(targetEntity = FileInfo.class,cascade = CascadeType.ALL,fetch = FetchType.EAGER)
  private Set<FileInfo> fileInfoSet;
  public void addFileInfo(FileInfo fileInfo){
    if(fileInfoSet==null){
      fileInfoSet = new HashSet<>();
    }
    fileInfoSet.add(fileInfo);
  }
  public void removeFileInfo(FileInfo fileInfo){
    if(fileInfoSet==null){
      fileInfoSet = new HashSet<>();
    }
    fileInfoSet.remove(fileInfo);
  }
}
