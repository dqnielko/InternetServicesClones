package com.example.storageservice.dto;

import com.example.storageservice.entity.FileInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileInfoDto {
  private String name;
  private String date;
  public FileInfoDto(FileInfo fileInfo){
    name = fileInfo.getName();
    date = fileInfo.getDate().toString();
  }
}
