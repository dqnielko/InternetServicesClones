package com.example.storageservice.service;

import com.example.storageservice.dto.FileInfoDto;
import com.example.storageservice.entity.FileInfo;
import com.example.storageservice.entity.StorageUser;
import com.example.storageservice.repository.FileInfoRepository;
import com.example.storageservice.repository.StorageUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StorageUserService {
  @Value("${default.storage.folder}")
  private String storageFolder;
  private final StorageUserRepository storageUserRepository;
  private final FileInfoRepository fileInfoRepository;
  public boolean addFile(String username, MultipartFile multipartFile) throws IOException {
    boolean folderCreated = createUserFolderIfNotExist(username);
    boolean result = false;
    if(folderCreated && multipartFile!=null && !multipartFile.isEmpty()){
      String filename = multipartFile.getOriginalFilename();
      String path = storageFolder+username+"/"+filename;
      multipartFile.transferTo(new File(path));
      saveFileInfoIntoDatabase(username, filename, path);
      result = true;
    }
    return result;
  }
  public Set<FileInfoDto> getFilesOfUser(String username){
    Optional<StorageUser> storageUserOptional = storageUserRepository.findByUsername(username);
    Set<FileInfoDto> fileInfoDtoSet = new HashSet<>();
    if(storageUserOptional.isPresent()){
      Set<FileInfo> fileInfoSet = storageUserOptional.get().getFileInfoSet();
      fileInfoDtoSet = fileInfoSet.stream().map(FileInfoDto::new).collect(Collectors.toSet());
    }
    return fileInfoDtoSet;
  }
  @Transactional
  public boolean deleteFileFromStorage(String username, String fileName){
    Optional<StorageUser> storageUserOptional = storageUserRepository.findByUsername(username);
    Optional<FileInfo> fileInfoOptional = fileInfoRepository.findByName(fileName);
    boolean result = false;
    if(storageUserOptional.isPresent() && fileInfoOptional.isPresent()){
      storageUserRepository
              .deleteFileInfoFromJointTable(storageUserOptional.get().getId(),fileInfoOptional.get().getId());
      fileInfoRepository.deleteByName(fileInfoOptional.get().getName());
      storageUserOptional.get().removeFileInfo(fileInfoOptional.get());
      boolean fileDeleted = deleteFileFromStorage(fileInfoOptional.get().getPath());
      if(fileDeleted){
        result = true;
      }
    }
    return result;
  }
  public ByteArrayResource getFileResource(String username, String fileName)throws IOException{
    String filePath = getFilePath(username, fileName);
    ByteArrayResource resource=null;
    if(!Objects.equals(filePath, "")){
      Path path = Paths.get(filePath);
      try{
        resource= new ByteArrayResource(Files.readAllBytes(path));
      }catch (MalformedURLException malformedURLException){
        malformedURLException.printStackTrace();
      }
    }
    return resource;
  }
  private String getFilePath(String username, String fileName){
    String fullPath = storageFolder+username+"/"+fileName;
    Optional<FileInfo> fileInfoOptional = fileInfoRepository.findByPath(fullPath);
    if(fileInfoOptional.isEmpty()){
      fullPath = "";
    }
    return fullPath;
  }
  private boolean deleteFileFromStorage(String path){
    return new File(path).delete();
  }
  private void saveFileInfoIntoDatabase(String username, String filename, String path) {
    FileInfo fileInfo = fileInfoRepository.save(FileInfo.builder().name(filename).path(path).date(new Date()).build());
    Optional<StorageUser> storageUserOptional = storageUserRepository.findByUsername(username);
    StorageUser storageUser;
    if(storageUserOptional.isEmpty()){
      storageUser = StorageUser.builder().username(username).build();
      storageUser = storageUserRepository.save(storageUser);
    }else{
      storageUser = storageUserOptional.get();
    }
    storageUser.addFileInfo(fileInfo);
    storageUserRepository.save(storageUser);
  }

  private boolean createUserFolderIfNotExist(String username){
    String userStoragePath = storageFolder+username;
    Path path = Paths.get(userStoragePath);
    boolean result = true;
    if(!Files.exists(path)){
      result = new File(userStoragePath).mkdirs();
    }
    return result;
  }
}
