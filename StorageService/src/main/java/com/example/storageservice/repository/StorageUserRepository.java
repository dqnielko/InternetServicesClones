package com.example.storageservice.repository;

import com.example.storageservice.entity.StorageUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface StorageUserRepository extends JpaRepository<StorageUser,Long> {
  Optional<StorageUser> findByUsername(String username);
  @Query(value = "delete from storage_user_file_info_set where storage_user_id=:userId and " +
          "file_info_set_id=:fileInfoId",nativeQuery = true)
  @Modifying
  void deleteFileInfoFromJointTable(@Param("userId") Long userid,
                                    @Param("fileInfoId") Long fileInfoId);
}
