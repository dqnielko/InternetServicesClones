package com.example.storageservice.repository;

import com.example.storageservice.entity.FileInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileInfoRepository extends JpaRepository<FileInfo,Long> {
  Optional<FileInfo> findByName(String name);
  @Query("delete from FileInfo fileinfo where fileinfo.name=:fileName")
  @Modifying
  void deleteByName(@Param("fileName") String fileName);
  @Query("select fileInfo from FileInfo fileInfo where fileInfo.path=:path")
  Optional<FileInfo> findByPath(@Param("path") String path);
}
