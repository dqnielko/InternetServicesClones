package com.example.storageservice.security.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthUser {
  @Id
  @GeneratedValue
  private Long id;
  @Column
  private String username;
  @Column
  private String password;
  @OneToMany(targetEntity = Role.class,cascade = CascadeType.ALL,fetch = FetchType.EAGER)
  private Set<Role> roles = new HashSet<>();
  public void addRole(Role role){
    roles.add(role);
  }
}
