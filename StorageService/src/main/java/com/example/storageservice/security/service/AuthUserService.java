package com.example.storageservice.security.service;


import com.example.storageservice.security.dto.AuthUserDto;
import com.example.storageservice.security.entity.Role;
import com.example.storageservice.security.repository.AuthUserRepository;
import com.example.storageservice.security.entity.AuthUser;
import com.example.storageservice.security.entity.AuthUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthUserService implements UserDetailsService {
  private final AuthUserRepository authUserRepository;
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<AuthUser> authUserOptional = authUserRepository.findByUsername(username);
    AuthUserDetails authUserDetails;
    if(authUserOptional.isPresent()){
      authUserDetails = AuthUserDetails.fromAuthUser(authUserOptional.get());
    }else {
      throw new UsernameNotFoundException("There is no user with username:"+username);
    }
    return authUserDetails;
  }
}
