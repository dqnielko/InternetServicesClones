package com.example.storageservice.exception;

public class FileUploadingException extends Exception{
  public FileUploadingException(String message){
    super(message);
  }
}
