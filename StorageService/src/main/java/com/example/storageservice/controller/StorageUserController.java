package com.example.storageservice.controller;

import com.example.storageservice.dto.FileInfoDto;
import com.example.storageservice.exception.FileUploadingException;
import com.example.storageservice.service.StorageUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.security.Principal;
import java.util.Set;

@RestController
@RequestMapping("storage")
@RequiredArgsConstructor
public class StorageUserController {
  private final StorageUserService storageUserService;
  @PostMapping(path = "upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  @PreAuthorize("hasRole('ROLE_USER')")
  public ResponseEntity<String> addFile(@RequestParam("file")MultipartFile multipartFile, Principal principal)
          throws FileUploadingException {
    ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
    try{
      if(storageUserService.addFile(principal.getName(),multipartFile)){
        responseEntity = ResponseEntity.ok().build();
      }
    }catch (IOException ioException){
      throw new FileUploadingException("Couldn't upload file for user:"+principal.getName());
    }
    return responseEntity;
  }
  @GetMapping("files")
  @PreAuthorize("hasRole('ROLE_USER')")
  public ResponseEntity<Set<FileInfoDto>> getFiles(Principal principal){
    Set<FileInfoDto> fileInfoDtoSet = storageUserService.getFilesOfUser(principal.getName());
    return ResponseEntity.ok(fileInfoDtoSet);
  }
  @DeleteMapping("/file")
  @PreAuthorize("hasRole('ROLE_USER')")
  public ResponseEntity<String> deleteFile(@RequestParam("name") String fileName, Principal principal){
    System.out.println(fileName);
    boolean fileDeleted = storageUserService.deleteFileFromStorage(principal.getName(),fileName);
    ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
    if(fileDeleted){
      responseEntity = ResponseEntity.ok().build();
    }
    return responseEntity;
  }
  @GetMapping("/file/{fileName}")
  @PreAuthorize("hasRole('ROLE_USER')")
  public ResponseEntity<?> downloadFile(@PathVariable String fileName, Principal principal)throws IOException{
    ByteArrayResource resource = storageUserService.getFileResource(principal.getName(),fileName);
    ResponseEntity<?> responseEntity = ResponseEntity.notFound().build();
    if(resource!=null){
      responseEntity = ResponseEntity.ok()
              .contentLength(resource.contentLength())
              .contentType(MediaType.APPLICATION_OCTET_STREAM)
              .body(resource);
    }
    return responseEntity;
  }
}
