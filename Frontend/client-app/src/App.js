import './App.css';
import axios from "axios";
import ReactDOM from 'react-dom';

let jwt = null;
let authHeader = null;
let serverUrl = "http://localhost:8080/";
let registerSuccessful = "";
let authElementDisplay='block';
let filesList = [];
let addingFileButtonDisplay = 'none';
function App() {
  return (
    <div>
      <AuthenticationInputs></AuthenticationInputs>
      <LoginCaller></LoginCaller>
      <RegisterCaller></RegisterCaller>
      <RegisterSuccessful></RegisterSuccessful>
      <AddingFileButton></AddingFileButton>
      <FilesDivs></FilesDivs>
    </div>
  );
}
function AddingFileButton(){
  return (<div className='addingFileButton' style={{display:addingFileButtonDisplay}}>
    <input type='file' id='fileInput'></input>
    <button onClick={uploadFile}>+</button>
  </div>)
}
function FilesDivs(){
  let renderedFilesList = filesList.map(file=><div className='fileListElement'>
    {file.name}{" "}{file.date}
      <a id={file.name} onClick={downloadFile} style={{color:"blue"}}> Download</a>
      <a id={file.name} onClick={deleteFile} style={{color:"red"}}> Delete</a>
    </div>);
  return (
    <div className='filesList'>
      {renderedFilesList}
    </div>
  );
}

function downloadFile(event){
  let fileName = event.currentTarget.id;
  fetch(serverUrl+"storage/file/"+fileName,{method:"GET",headers:{"Authorization":authHeader}})
  .then((response) => response.blob())
  .then((blob) => {
  // 2. Create blob link to download
    const url = window.URL.createObjectURL(new Blob([blob]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', fileName);
    // 3. Append to html page
    document.body.appendChild(link);
    // 4. Force download
    link.click();
    // 5. Clean up and remove the link
    link.parentNode.removeChild(link);
  })
}

function deleteFile(event){
  console.log(event.currentTarget.id);
  let fileName = event.currentTarget.id;
  fetch(serverUrl+"storage/file?name="+fileName,{method:'DELETE',headers:{"Authorization":authHeader}})
  .then(res=>{
    if(res.status==200){
      getFilesList();
    }
  });
}
function uploadFile(){
  let file = document.getElementById('fileInput').files[0];
  console.log(file);
  let formData = new FormData();
  formData.append('file',file);
  fetch(serverUrl+'storage/upload',{method:'POST',body:formData,
    headers:{"Authorization":authHeader}}).then(res=>{
  if(res.status==200)
    getFilesList();
});
  
}
function getFilesList(){
  filesList = [];
  axios({
    url:serverUrl+"storage/files",
    headers:{
      'Authorization':authHeader
    },
    method:"GET"
  }).then(res =>{
    extracFilesListFromJson(res.data);
    renderAddingFileButton();
    renderFilesList();
  });
}
function renderAddingFileButton(){
  addingFileButtonDisplay = 'block';
  renderApp();
}
function renderApp(){
  const root = document.getElementById('root');
  ReactDOM.render(<App></App>,root);
}
function renderFilesList(){
  renderApp();
}
function extracFilesListFromJson(json){
  for(let i = 0;i<json.length;i++){
    filesList.push({name:json[i]['name'],date:json[i]['date']});
  }
}
function RegisterSuccessful(){
  return (<div className='register-successful'>
    {registerSuccessful}
  </div>)
}
function AuthenticationInputs(){
  return (
    <div className='authInputs' style={{display:authElementDisplay}}>
      <p>Login</p>
      <input id='loginInput'></input>
      <p>Password</p>
      <input id='passwordInput'></input>
    </div>
  )
}
function attemptLogin(){
    let login = document.getElementById('loginInput').value;
    let password = document.getElementById('passwordInput').value;
    if(login!=null && password != null){
      axios({
        url:serverUrl+"login",
        method:"POST",
        headers:{
          "content-type":"application/json"
        },
        data:JSON.stringify({"username":login,"password":password})
      }).then((res)=>{
        if(res.status==200){
          jwt = res.data;
          authHeader = "Bearer "+jwt;
          document.getElementsByClassName('authInputs')[0].style.display='none';
          console.log(authHeader);
          hideAuthInputs();
          getFilesList();
        }
      }).catch(error=>{
        console.log(error);
      }).finally(()=>{
        registerSuccessful = "";
        const root = document.getElementById('root');
        ReactDOM.render(<App></App>,root);
      });
    }
}
function attemptRegister(){
  let login = document.getElementById('loginInput').value;
  let password = document.getElementById('passwordInput').value;
  if(login!=null && password!=null){
    axios({
      url:serverUrl+"register",
      method:"POST",
      headers:{
        "content-type":"application/json"
      },
      data:JSON.stringify({"username":login,"password":password})
    }).then((res)=>{
      if(res.status==200){
        registerSuccessful="Register successful!";
      }
    }).catch(error=>{
      console.log(error);
      registerSuccessful="Couldn't register. User already exists";
   }).finally(()=>{
    const root = document.getElementById('root');
    ReactDOM.render(<App></App>,root);
   });
  }
}
function hideAuthInputs(){
  authElementDisplay='none';
  let root = document.getElementById('root');
  ReactDOM.render(<App></App>,root);
}
function LoginCaller(){
  return (
    <div className="login-caller" style={{display:authElementDisplay}}>
      <button id="loginCallerButton" onClick={attemptLogin}>Login</button>
    </div>
  )
}
function RegisterCaller(){
  return (
    <div className="register-caller" style={{display:authElementDisplay}}>
      <button id="registerCallerButton" onClick={attemptRegister}>Register</button>
    </div>
  )
}

export default App;
